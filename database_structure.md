# Form-Fusion database structure

[[_TOC_]]

## wp_ff_form 

Contains the forms that exist within Form Fusion.

* id
    * The form ID.
* title
    *  The form title.
* date_created
    * The date that the form was created.
* is_active
    * If the form is active. Integer is used as a boolean (0 is false, 1 is true).
* is_trash
    * If the form is trashed. Integer is used as a boolean (0 is false, 1 is true).


## wp_ff_form_meta (amikor elmentettük a form szerkesztését)

Contains metadata associated with forms

* form_id
    * The form ID that the metadata is associated with.
* display_meta (metákon belüli kinézetet tartalmazza) 
    * Meta related to how the form and fields are configured.
* confirmations
    * Confirmation configuration.
* notifications
    * Notification configuration.


## wp_ff_form_view (statisztika miatt) 

Contains details on form views.

* id
    * The ID.
* form_id
    * The form ID that the data is associated with.
* date_created
    * The date that the row was created.
* ip
    * The IP that accessed the form.
* count
    * The number of times that the IP accessed the form.

## wp_ff_entry

Contains Form Fusion entries.

* id
    * The unique entry ID.
* form_id
    * The form ID that the entry is associated with.
* date_created
    * The date that the entry was created.
* date_updated
    * The date that the entry was last updated.
* is_starred
    * If the entry is starred. Integer is used as a boolean (0 is false, 1 is true).
* is_read
    * If the entry has been marked as read. Integer is used as a boolean (0 is false, 1 is true).
* ip
    * The IP address that submitted the entry.
* source_url
    * The URL of where the submission took place.
* user_agent
    * The user agent of the entry submitter.
* payment_id 
    * Payment ID
* created_by 
    * Filled in by (user_id)
* status
    * Status of the submitted entry

## wp_ff_entry_payment

* id
    * The ID.
* currency
    * The currency type. 
* payment_status
    *   The status of the payment, if applicable.
* payment_date
    * The date that the payment took place, if applicable.
* payment_amount
    * The amount of the payment, if applicable.
* payment_method
    * The transaction method used to process the payment, if applicable.
* transaction_id
    * The transaction ID associated with the entry, if applicable.
* is_fulfilled
    * If the transaction has been fulfilled, if applicable.
* user_id
    * The ID of the user that created the entry, if applicable.
* transaction_type
    * The transaction type, if applicable.
* status
    * The current entry status.

## wp_ff_entry_meta

Contains additional metadata related to entries. Details from fields as well as add-ons are stored here.


* id
    * The unique ID.
* entry_id
    * The entry ID that the meta is 
* form_id
    * The form ID that the entry meta is associated with.
associated with.
* meta_key
    * The meta key. 
* meta_value
    * The value stored under the meta key.
* item_index
    * The item index.

## wp_ff_form_meta_style 
 Styles related to metadata
* id
    * The ID
* meta_id
    * Which meta it belongs to
* style
    * Style associated with metadata "field styling"

## wp_entry_notes

Comments/notifications related to submitted entries

* id 
    * The ID
* entry_id
    * which entry it belongs to
* user_name
    * User name of the submitter
* user_id
    * User ID of the submitter
* data_created
    * Date of comment creation
* value
    * Message/comment content
* note_type
    * Comment or notification
* sub_type
    * Success feedback



